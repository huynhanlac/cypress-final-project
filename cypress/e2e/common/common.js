import { ApiConstants } from "../../constants/api-constants";
import { UrlConstants } from "../../constants/url-constants";

import { Given } from "@badeball/cypress-cucumber-preprocessor";

Given("I logged in Unsplash application", () => {
    cy.fixture("test-account").then(function (userFixture) {
        cy.createRequest('GET', ApiConstants.USER_PROFILE_ENDPOINT).then((response) => {
            cy.visit(UrlConstants.LOGIN_URL)
            cy.login(userFixture.LacHuynh.userEmail, userFixture.LacHuynh.userPassword)
            cy.visit(UrlConstants.LIKES_URL.replace('%s', response.body.username))
        })
    })
})
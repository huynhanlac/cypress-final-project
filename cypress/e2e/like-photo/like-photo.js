import { When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { ApiConstants } from "../../constants/api-constants";
import { UrlConstants } from "../../constants/url-constants";
import { LikesPage } from "../../pages/likes-page";

// let linkDownloadPhoto = [];
let randomPhotoId = [];
let likePhotoId  = [];

When("I like 3 random photos",()=>{
    for (let i = 0; i < 3; i++) {
        cy.createRequest('GET', ApiConstants.GET_RANDOM_PHOTO_ENDPOINT)
          .then(({ body }) => {
            randomPhotoId.push(body.id)
            likePhotoId.push('/photos/'+randomPhotoId[i])
            cy.createRequest('POST', ApiConstants.LIKE_PHOTO_ENDPOINT.replace('%s', body.id))
        })
    }
    cy.log(randomPhotoId)
})
    

Then("Total liked photos should be equal 3",()=>{
    cy.get("@username").then((username)=> {
      cy.visit(UrlConstants.LIKES_URL.replace('%s', username))
      LikesPage.getActualTotalLikes().should('have.text', '3')
  })  
})

Then("3 photos appear in Likes section",()=>{
    for (let j = 0; j < 3; j++) {
      LikesPage.getLinkPhotoByOrder(j+1).should('have.text',likePhotoId[j]) 
    }
})

before(() => {
  cy.createRequest('GET', ApiConstants.USER_PROFILE_ENDPOINT).then((response) => {
    const username = response.body.username
    cy.wrap(username).as("username")
    cy.createRequest('GET', ApiConstants.USER_LIKE_PHOTO_ENDPOINT.replace('%s', username)).then((response) => {
      response.body.forEach((photo) => {
        cy.createRequest('DELETE', ApiConstants.LIKE_PHOTO_ENDPOINT.replace('%s', photo.id))
      })
    })
  })
})


Feature: Like photo
    I want to like a photo at homepage application
  Scenario: Like 3 random photos successfully
    Given I logged in Unsplash application
    When I like 3 random photos
    Then Total liked photos should be equal 3
    And 3 photos appear in Likes section
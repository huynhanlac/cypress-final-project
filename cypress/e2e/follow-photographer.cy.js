const { UrlConstants } = require('../constants/url-constants')
const { HomePage } = require('../pages/home-page')
const { PhotoDetailPage } = require('../pages/photo-detail-page')
const { ColorConstants } = require('../constants/color-constants')


describe('follow photographer', function () {
    beforeEach(function () {
        cy.visit(UrlConstants.LOGIN_URL)
        cy.login(Cypress.env('userEmail'), Cypress.env('userPassword'))
    })

    it('follow photographer successfully', function () {
        HomePage.clickFirstImg()
        PhotoDetailPage.hoverOnIconBtn()
        PhotoDetailPage.clickFollowBtn()
        PhotoDetailPage.getFollowBtn()
            .should('have.css', 'background-color')
            .and('eq', ColorConstants.FOLLOW_BUTTON_COLOR)
        PhotoDetailPage.getFollowBtn()
            .should('have.attr', 'title')
            .and('eq', 'Following')
    })

    afterEach(() => {
        PhotoDetailPage.clickFollowBtn()
    })
})
const LST_ITEM_IN_COLLECTION = "//div[@class='ripi6']";
const COLLECTION = "//a[contains(@href,'lJku4edK_cs')]";
const FISRT_IMG = "//img[@itemprop='thumbnailUrl'][1]";
const BTN_ADD_TO_COLLECTION = "//div[@data-test='photos-route']//header/div//button[@title='Add to collection']";
const BTN_REMOVE_FROM_COLLECTION = "//h3[text()='Add to Collection']//following::ul//button";
const IMGS = "//figure[@itemprop='image']"

export const CollectionPage = {

    getItemsInCollection() {
        return cy.xpath(LST_ITEM_IN_COLLECTION)
    },

    clickOnMyCollection() {
        cy.xpath(COLLECTION).click()
    },
    goToMyCollection(collectionId) {
        cy.visit(`/collections/${collectionId}`)
    },
    clickOn1stImgInMyCollection() {
        cy.xpath(FISRT_IMG).click()
    },
    clickOnAddToCollectionBtn() {
        cy.xpath(BTN_ADD_TO_COLLECTION).click()
    },
    clickToRemoveFromCollection() {
        cy.xpath(BTN_REMOVE_FROM_COLLECTION).click()
    },
    getPhotoCountInCollection() {
        return cy.xpath(IMGS)
    }
}
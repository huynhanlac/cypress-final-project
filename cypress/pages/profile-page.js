const BTN_EDIT_PROFILE = "//a[text()='Edit profile']"
const LBL_FULLNAME = "//a[text()='Edit profile']//ancestor::div[4]/div[1]"
const LBL_BIO = "(//a[text()='Edit profile']//ancestor::div[5]//following::div[4]/div/div)[1]"
const LBL_LOCATION ="(//a[text()='Edit profile']//ancestor::div[5]//following::div[4]//div)[9]"
export const ProfilePage = {
    clickEditProfileBtn() {
        cy.xpath(BTN_EDIT_PROFILE).click()
    },

    getTextFullName() {
        return cy.xpath(LBL_FULLNAME)
    },
    getBio() {
        return cy.xpath(LBL_BIO)
    },
    getlocation() {
        return cy.xpath(LBL_LOCATION)
    }
}
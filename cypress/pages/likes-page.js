const LBL_TOTAL_LIKES = "//a[@data-test='user-nav-link-likes']//span[2]//span"
const BTN_LIKE_PHOTO_BY_ORDER = "//button[@title='Like'][%s]"
const BTN_LINK_PHOTO_BY_ORDER = "(//a[@itemprop='contentUrl'])[%s]"

export const LikesPage = {
    getActualTotalLikes() {
        return cy.xpath(LBL_TOTAL_LIKES)
    },

    getLinkPhotoByOrder(order) {
        return cy.xpath(BTN_LINK_PHOTO_BY_ORDER.replace('%s', order)).invoke('attr', 'href')
    },

    clickLikePhotoButtonByOrder(order) {
        cy.xpath(BTN_LIKE_PHOTO_BY_ORDER.replace('%s', order)).click()
    }
}
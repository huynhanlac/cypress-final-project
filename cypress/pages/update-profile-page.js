const TXT_FIRSTNAME = "input#user_first_name"
const TXT_LASTNAME = "input#user_last_name"
const TXT_EMAIL = "input#user_email"
const TXT_USERNAME = "input#user_username"
const TXT_LOCATION = "input#user_location"
const TXT_PERSONAL_SITE = "input#user_url"
const TXT_BIO = "textarea#user_bio"
const TXT_INTERESTS = "input#user_interests"
const TXT_INSTAGRAM_USERNAME = "input#user_instagram_username"
const TXT_TWITTER_USERNAME = "input#twitter_username"
const TXT_PAYPAL_EMAIL = "input#user_paypal"
const BTN_UPDATE_ACCOUNT = "input[value='Update account']"

export const UpdateProfilePage = {
    inputFirstName(firstName) {
        cy.get(TXT_FIRSTNAME).clear().type(firstName)
    },

    inputLastName(lastName) {
        cy.get(TXT_LASTNAME).clear().type(lastName)
    },

    inputEmail(email) {
        cy.get(TXT_EMAIL).clear().type(email)
    },

    inputUserName(userName) {
        cy.get(TXT_USERNAME).clear().type(userName)
    },

    inputLocation(location) {
        cy.get(TXT_LOCATION).clear().type(location)
    },

    inputPersonalSite(personalSite) {
        cy.get(TXT_PERSONAL_SITE).clear().type(personalSite)
    },

    inputBio(bioLink) {
        cy.get(TXT_BIO).clear().type(bioLink)
    },

    inputInterests(interests) {
        cy.get(TXT_INTERESTS).clear().type(interests)
    },

    inputInstagramUsername(instagramUsername) {
        cy.get(TXT_INSTAGRAM_USERNAME).clear().type(instagramUsername)
    },

    inputTwitterUsername(twitterUsername) {
        cy.get(TXT_TWITTER_USERNAME).clear().type(twitterUsername)
    },

    inputPaypalEmail(paypalEmail) {
        cy.get(TXT_PAYPAL_EMAIL).clear().type(paypalEmail)
    },

    clickUpdateAccountBtn() {
        cy.get(BTN_UPDATE_ACCOUNT).click()
    }
}
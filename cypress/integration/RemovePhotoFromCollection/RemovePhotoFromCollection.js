import { Given, When, Then, After, Before } from "cypress-cucumber-preprocessor/steps";
const { UrlConstants } = require('../../constants/url-constants')
const { CollectionPage } = require('../../pages/collection-page')
const { ApiConstants } = require('../../constants/api-constants');
let randomPhotoId = [];
let collectionId;
let username;
Given(`There are 2 photos in collection`, () => {
    cy.createRequest('GET', ApiConstants.USER_PROFILE_ENDPOINT).then((response) => {
        username = response.body.username
        cy.log(username)

        let url = ApiConstants.GET_USER_COLLECTIONS.replace('%s', username)
       
        cy.createRequest('GET', url, null).then((response) => {
            collectionId = response.body[0].id

            for (let i = 0; i < 2; i++) {
                
                cy.createRequest('GET', ApiConstants.GET_RANDOM_PHOTO_ENDPOINT)
                    .then(({ body }) => {
                        randomPhotoId.push(body.id)
                        const payload = {
                            photo_id: `${body.id}`
                        }
                        let url = ApiConstants.ADD_PHOTO_2_COLLECTION_ENDPOINT.replace('%s', collectionId)

                        
                        cy.createRequest('POST', url, payload)
                    })
            }
        })
    })
});
Given(`I login Unsplash account`, () => {
    cy.visit(UrlConstants.LOGIN_URL)
    cy.login(Cypress.env('userEmail'), Cypress.env('userPassword'))
    CollectionPage.goToMyCollection(collectionId)
});

When(`I remove 1 photo from the collection`, () => {
    const payload = {
        photo_id: `${randomPhotoId[1]}`
      }
      let url = ApiConstants.REMOVE_PHOTO_FROM_COLLECTION.replace('%s', collectionId)
      cy.createRequest('DELETE', url, payload).then(() => {
        CollectionPage.goToMyCollection(collectionId)
        CollectionPage.getPhotoCountInCollection().should('have.length', 1)
      })
});

Then(`there is 1 remaining photo in collection`, () => {
    CollectionPage.goToMyCollection(collectionId)
    CollectionPage.getPhotoCountInCollection().should('have.length', 1)
});
After(() => {
    const payload = {
        photo_id: `${randomPhotoId[0]}`
      }
      let url = ApiConstants.REMOVE_PHOTO_FROM_COLLECTION.replace('%s', collectionId)
      cy.createRequest('DELETE', url, payload)
});
Feature: Remove photo from collection
  Scenario: Remove photo from collection successfully
    Given There are 2 photos in collection
    Given I login Unsplash account
    When I remove 1 photo from the collection
    Then there is 1 remaining photo in collection
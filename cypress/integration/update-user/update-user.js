import { Given, When, Then, After } from "cypress-cucumber-preprocessor/steps";
const { UrlConstants } = require('../../constants/url-constants')
const { HomePage } = require('../../pages/home-page')



Given(`I login Unsplash account`, () => {
    cy.visit(UrlConstants.LOGIN_URL)
    cy.login(Cypress.env('userEmail'), Cypress.env('userPassword'))
});

And("I go to profile page",()=>{
    HomePage.navigateProfilePage()
});

When("I click edit tag link",()=>{
    ProfilePage.clickEditProfileBtn()
});

When("I edit all information",()=>{
    UpdateProfilePage.inputFirstName(this.newInfo.firstName)
    UpdateProfilePage.inputLastName(this.newInfo.lastName)
    UpdateProfilePage.inputEmail(this.newInfo.email)
    UpdateProfilePage.inputUserName(this.newInfo.userName)
    UpdateProfilePage.inputLocation(this.newInfo.location)
    UpdateProfilePage.inputPersonalSite(this.newInfo.personalSite)
    UpdateProfilePage.inputBio(this.newInfo.bio)
    UpdateProfilePage.inputInstagramUsername(this.newInfo.instagramUsername)
    UpdateProfilePage.inputTwitterUsername(this.newInfo.twitterUsername)
    UpdateProfilePage.inputPaypalEmail(this.newInfo.paypalEmail)
});

when("I click Update Account button",()=>{
    UpdateProfilePage.clickUpdateAccountBtn()
})

Then("I go to Profile Page",()=>{
    cy.visit("/@" + this.newInfo.userName)
})

Then("My full name is displayed",()=>{
    var fullName = this.newInfo.firstName + " " + this.newInfo.lastName
    ProfilePage.getTextFullName().should('have.text', fullName)
})

After(() => {
    cy.updateDefaultUsername()
} )

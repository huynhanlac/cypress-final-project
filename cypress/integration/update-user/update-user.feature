Feature: Editing profile
  I want to edit my profile
  Scenario: edit profile successfully
    Given I logged into the application
    And  I go to profile page
    When I click edit tags link
    When I edit all information of user
    When I click Update Account button
    Then i go to Profile Page
    Then My full name is displayed
Feature: Following a photographer
  I want to follow a photographer
  Scenario: Follow successfully
    Given I login Unsplash account
    When I click first picture in homepage
    When I hover the avatar image
    When I click follow button
    Then follow button change color
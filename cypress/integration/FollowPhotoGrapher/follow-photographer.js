import { Given, When, Then, After } from "cypress-cucumber-preprocessor/steps";
const { UrlConstants } = require('../../constants/url-constants')
const { HomePage } = require('../../pages/home-page')
const { PhotoDetailPage } = require('../../pages/photo-detail-page')
const { ColorConstants } = require('../../constants/color-constants')
Given(`I login Unsplash account`, () => {
    cy.visit(UrlConstants.LOGIN_URL)
    cy.login(Cypress.env('userEmail'), Cypress.env('userPassword'))
});

When(`I click first picture in homepage`, () => {
    HomePage.clickFirstImg()
});

When(`I hover the avatar image`, () => {
    PhotoDetailPage.hoverOnIconBtn()
});

When(`I click follow button`, () => {
    PhotoDetailPage.clickFollowBtn()
});

Then(`follow button change color`, () => {
    PhotoDetailPage.getFollowBtn()
        .should('have.css', 'background-color')
        .and('eq', ColorConstants.FOLLOW_BUTTON_COLOR)
    PhotoDetailPage.getFollowBtn()
        .should('have.attr', 'title')
        .and('eq', 'Following')
});

After(() => {
    PhotoDetailPage.clickFollowBtn()
});
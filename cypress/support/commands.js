// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })


const { LoginPage } = require("../pages/login-page")
Cypress.Commands.add('login', (email, password) => {
  LoginPage.inputUserEmail(email)
  LoginPage.inputPassword(password)
  LoginPage.clickLoginBtn()
})

Cypress.Commands.add('createRequest',(method, url, body, options={})=>{
  cy.request({
      method: method,
      url: Cypress.env('baseApiUrl') + url,
      auth: {
        'bearer': Cypress.env('token'),
      },
      body: body

    })
})

Cypress.Commands.add('updateDefaultUsername', function() {
cy.request({
    method: 'PUT',
    url: Cypress.env('baseApiUrl') + ApiConstants.USER_PROFILE_ENDPOINT, 
    auth: {
        'bearer': Cypress.env('token'),
    },
    body: {
      'username': Cypress.env('userName'),
      'first_name': "dang",
      'last_name': "duong",
      'location': "",
      'bio': "",
    },
  })
})


require ('cypress-downloadfile/lib/downloadFileCommand')
require('cy-verify-downloads').addCustomCommand();



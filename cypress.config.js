const { defineConfig } = require("cypress");
const createBundler = require("@bahmutov/cypress-esbuild-preprocessor");
const preprocessor = require("@badeball/cypress-cucumber-preprocessor");
const createEsbuildPlugin = require("@badeball/cypress-cucumber-preprocessor/esbuild");
const {downloadFile} = require('cypress-downloadfile/lib/addPlugin');
const { isFileExist, findFiles } = require('cy-verify-downloads');
const fs = require('fs');

async function setupNodeEvents(on, config) {
  const cucumber = require('cypress-cucumber-preprocessor').default
  on('file:preprocessor', cucumber());
  await preprocessor.addCucumberPreprocessorPlugin(on, config);
  on(
    "file:preprocessor",
    createBundler({
      plugins: [createEsbuildPlugin.default(config)],
    })
  );
  return config;
}
module.exports = defineConfig({
  e2e: {
    specPattern: ["**/*.feature", "**/integration/*.feature"],
    supportFile: false,
    setupNodeEvents,
    baseUrl: "https://unsplash.com",
    chromeWebSecurity: false,
    video: false,
    watchForFileChanges: false,
    env: {
      baseApiUrl: 'https://api.unsplash.com',
      userEmail: 'anlachuynh@gmail.com',
      userPassword: 'Dauphong1907',
      userName: "huynhanlac1",  
      token:"-QEt-cTAEcsvTX64_Fe4A-VsKSJDufdBZcccikxBp3w",
    }
  }
});
